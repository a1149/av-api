import Router from 'av-core-express/lib/router'
import { statusControler, nodeController } from '@controllers'

Router.route('/live').get(statusControler)

Router.route('/interactions/inicialize').post(nodeController)

Router.route('/interactions').post(nodeController)

export default Router
