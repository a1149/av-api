import { getNode, getIntegration } from '@app/actions'
import { Request as ReqExpress, Response as ResExpress } from 'av-core-express/lib/interfaces'
import { Response as ResponseHandler } from 'av-core-express/lib/handlers'
import { validateRequestGetNode, formatedNodeResponse } from '@root/app/helpers'

export default async (req: ReqExpress, res: ResExpress): Promise<ResExpress> => {
  const Response = new ResponseHandler()
  try {
    await validateRequestGetNode(req.body)

    const reqBody = req.body

    const node = await getNode(reqBody)
    const integration = await getIntegration(reqBody, node)
    const formatedData = await formatedNodeResponse(integration, node, reqBody)
    const response = Response.success(formatedData)
    return res.status(response.statusCode).json(response)
  } catch (error) {
    const codeError = error.isJoi ? 'INVALID_BODY' : 'UNCAUGHT_EXCEPTION'
    const errorRes = Response.error(codeError, error)
    return res.status(errorRes.statusCode).json(errorRes)
  }
}
