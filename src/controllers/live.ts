import { Request, Response } from 'express'

export default (request: Request, response: Response): Response => response.status(200).json({ status: 'active' })
