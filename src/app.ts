import { routes } from '@routes'
import { unknownRoutesMiddleware } from 'av-core-express/lib/middlewares'
import app from 'av-core-express'

app.use('/', routes)
app.use(unknownRoutesMiddleware)

export default app
