import { INode } from '@app/interfaces'

export default <INode[]>[
  {
    id: 'CHIT_hola_AV',
    option_name: '',
    message: 'Hola {#NombreCliente}, me llamo Santi y soy tu Asistente Virtual. 🤖 Para poder ayudarte, seleccioná el motivo por el cual querés consultarme 👇',
    children: [
      'CLAV_menu_AV',
      'CTAS_menu_AV',
      'TARJ_menu_AV',
      'PRES_menu_AV',
      'GRAL_menu_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'CHIT_hola_AV'
  },
  {
    id: ' ',
    option_name: '',
    message: 'Para mas información visitá nuestro {#!data1}. Podés volver a consultarme por otro tema.',
    children: [
      'CLAV_menu_AV',
      'CTAS_menu_AV',
      'TARJ_menu_AV',
      'PRES_menu_AV',
      'GRAL_menu_AV'
    ],
    data: [
      {
        text: 'centro de ayuda',
        link: 'https://ayuda.santander.com.ar',
        type: 'url'
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'CHIT_adios_AV'
  },
  {
    id: 'CLAV_menu_AV',
    option_name: 'Claves',
    message: 'Elegí alguna de las siguientes opciones',
    children: [
      'CLAV_santander_menu_AV',
      'CLAV_token_coordenadas_AV',
      'CLAV_clave_fiscal_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'CLAV_menu_AV'
  },
  {
    id: 'CTAS_menu_AV',
    option_name: 'Cuentas',
    message: 'Elegí alguna de las siguientes opciones',
    children: [
      'CTAS_menu_informacion_AV',
      'CTAS_menu_movimientos_AV',
      'CTAS_menu_pago_transferencia_AV',
      'CTAS_menu_alta_baja_AV',
      'CTAS_menu_cheques_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'CTAS_menu_AV'
  },
  {
    id: 'GRAL_menu_AV',
    option_name: 'Otras Consultas',
    message: 'Elegí alguna de las siguientes opciones',
    children: [
      'GRAL_turnos_menu_AV',
      'GRAL_telefonos_utiles_AV',
      'INVE_compra_venta_moneda_extranjera_AV',
      'GRAL_datos_cliente_modificacion_AV',
      'INVE_menu_AV',
      'GRAL_superclub_como_funciona_AV',
      'GRAL_inconveniente_menu_AV',
      'GRAL_baja_producto_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'GRAL_menu_AV'
  },
  {
    id: 'PRES_menu_AV',
    option_name: 'Préstamos',
    message: 'Elegí alguna de las siguientes opciones',
    children: [
      'PRES_solicitar_AV',
      'PRES_pago_de_cuota_AV',
      'PRES_precancelacion_AV',
      'PRES_valor_tasa_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'PRES_menu_AV'
  },
  {
    id: 'TARJ_menu_AV',
    option_name: 'Tarjetas',
    message: 'Elegí alguna de las siguientes opciones',
    children: [
      'TARJ_delivery_menu_AV',
      'TARJ_menu_general_AV',
      'TARJ_menu_pago_AV',
      'TARJ_menu_solic_tramite_AV',
      'TARJ_menu_alta_baja_AV',
      'TARJ_menu_inconveniente_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'TARJ_menu_AV'
  },
  {
    id: 'CLAV_santander_menu_AV',
    option_name: 'Claves Santander',
    message: 'Hace clic para conocer la información',
    children: [
      'CLAV_banelco_pin_AV',
      'CLAV_canales_AV',
      'CLAV_banelco_alfa_AV',
      'CLAV_pin_credito_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'CLAV_santander_menu_AV'
  },
  {
    id: 'CTAS_menu_alta_baja_AV',
    option_name: 'Altas / Bajas',
    message: 'Hace clic para conocer la información',
    children: [
      'CTAS_alta_nueva_AV',
      'CTAS_upgrade_AV',
      'CTAS_baja_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'CTAS_menu_alta_baja_AV'
  },
  {
    id: 'CTAS_menu_cheques_AV',
    option_name: 'Cheques / eCheq',
    message: 'Hace clic para conocer la información',
    children: [
      'CTAS_cheques_AV',
      'CTAS_echeq_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'CTAS_menu_cheques_AV'
  },
  {
    id: 'CTAS_menu_informacion_AV',
    option_name: 'Información general',
    message: 'Hace clic para conocer la información',
    children: [
      'CTAS_comisiones_AV',
      'CTAS_cbu_alias_AV',
      'CTAS_comprobantes_AV',
      'CTAS_mi_ejecutivo_de_cuenta_AV',
      'CTAS_migracion_sucursal_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'CTAS_menu_informacion_AV'
  },
  {
    id: 'CTAS_menu_movimientos_AV',
    option_name: 'Movimientos',
    message: 'Hace clic para conocer la información',
    children: [
      'CTAS_consulta_saldo_movimientos_AV',
      'CTAS_resumen_online_AV',
      'CTAS_deposito_acreditacion_AV',
      'CTAS_dudas_por_movimiento_AV',
      'CTAS_descubierto_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'CTAS_menu_movimientos_AV'
  },
  {
    id: 'CTAS_menu_pago_transferencia_AV',
    option_name: 'Pagos / Transferencias',
    message: 'Hace clic para conocer la información',
    children: [
      'CTAS_pago_y_debito_servicio_AV',
      'CTAS_pago_debito_inconveniente_AV',
      'CTAS_transferencias_AV',
      'CTAS_transferencia_acreditacion_AV',
      'CTAS_transferencia_inconveniente_AV',
      'CTAS_comex_transferencia_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'CTAS_menu_pago_transferencia_AV'
  },
  {
    id: 'GRAL_inconveniente_menu_AV',
    option_name: 'Inconvenientes',
    message: 'Hace clic para conocer la información',
    children: [
      'GRAL_reclamo_comision_AV',
      'GRAL_reclamo_promocion_AV',
      'GRAL_reclamo_producto_AV',
      'GRAL_reclamo_estado_AV',
      'GRAL_reclamo_otro_problema_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'GRAL_inconveniente_menu_AV'
  },
  {
    id: 'GRAL_turnos_menu_AV',
    option_name: 'Información sobre Turnos en Sucursal',
    message: 'Hace clic para conocer la información',
    children: [
      'GRAL_turnos_online_ejecutivo_AV',
      'GRAL_turnos_online_caja_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'GRAL_turnos_menu_AV'
  },
  {
    id: 'INVE_menu_AV',
    option_name: 'Inversiones',
    message: 'Hace clic para conocer la información',
    children: [
      'INVE_plazo_fijo_alta_AV',
      'INVE_informacion_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'INVE_menu_AV'
  },
  {
    id: 'TARJ_delivery_menu_AV',
    option_name: 'Envío de tarjeta',
    message: 'Hace clic para conocer la información',
    children: [
      'TARJ_delivery_tarjeta_estado_AV',
      'TARJ_reimpresion_renovacion_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'TARJ_delivery_menu_AV'
  },
  {
    id: 'TARJ_menu_alta_baja_AV',
    option_name: 'Alta / Baja',
    message: 'Hace clic para conocer la información',
    children: [
      'TARJ_tc_alta_nueva_AV',
      'TARJ_tc_adicional_alta_AV',
      'TARJ_debito_servicio_baja_AV',
      'TARJ_tc_baja_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'TARJ_menu_alta_baja_AV'
  },
  {
    id: 'TARJ_menu_general_AV',
    option_name: 'Información general',
    message: 'Hace clic para conocer la información ',
    children: [
      'TARJ_limites_disponibles_consulta_AV',
      'TARJ_ultimos_consumos_AV',
      'TARJ_promociones_vigentes_AV',
      'TARJ_sorpresa_alta_o_baja_AV',
      'TARJ_comisiones_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'TARJ_menu_general_AV'
  },
  {
    id: 'TARJ_menu_inconveniente_AV',
    option_name: 'Inconvenientes',
    message: 'Hace clic para conocer la información',
    children: [
      'TARJ_denuncia_extravio_o_robo_AV',
      'TARJ_dudas_consumo_AV',
      'TARJ_desconocimiento_consumo_AV',
      'TARJ_promocion_no_realizada_AV',
      'TARJ_autorizacion_compra_AV',
      'TARJ_bloqueo_AV',
      'TARJ_extraccion_inconveniente_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'TARJ_menu_inconveniente_AV'
  },
  {
    id: 'TARJ_menu_pago_AV',
    option_name: 'Pagos',
    message: 'Hace clic para conocer la información',
    children: [
      'TARJ_pago_AV',
      'TARJ_tc_stop_debit_AV',
      'TARJ_acreditacion_pago_AV',
      'TARJ_tc_fecha_vencimiento_cierre_cambio_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'TARJ_menu_pago_AV'
  },
  {
    id: 'TARJ_menu_solic_tramite_AV',
    option_name: 'Solicitudes y trámites',
    message: 'Hace clic para conocer la información',
    children: [
      'TARJ_limites_disponibles_ampliacion_AV',
      'TARJ_refinanciacion_AV',
      'TARJ_marca_viajero_AV',
      'TARJ_habilitacion_AV',
      'TARJ_extraccion_por_cajero_AV',
      'TARJ_envio_de_efectivo_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'TARJ_menu_solic_tramite_AV'
  },
  {
    id: 'CLAV_banelco_alfa_AV',
    option_name: 'Clave Alfabética Banelco',
    message: 'Para generar o blanquear tu clave alfabética hacé clic {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'perfil-blanqueo-clave-banelco',
        type: 'stack'
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'blanqueo-clave-banelco-alfanumerica',
    intent: 'CLAV_banelco_alfa_AV'
  },
  {
    id: 'CLAV_banelco_pin_AV',
    option_name: 'Clave Banelco (Tarjeta de Débito)',
    message: 'La clave Banelco consta de 4 dígitos (sólo números) y es la que usás para operar en cajeros automáticos con tu tarjeta de débito.\nSi no tenés clave, la olvidaste o bloqueaste para seguir operando podés blanquearla ingresando {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'perfil-blanqueo-clave-banelco',
        type: 'stack'
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'blanqueo-clave-banelco-numerica',
    intent: 'CLAV_banelco_pin_AV'
  },
  {
    id: 'CLAV_canales_AV',
    option_name: 'Clave / Usuario Santander (Online Banking / App / Telefónica)',
    message: 'Tu Clave y Usuario Santander te permite ingresar a Online Banking, la App y comunicarte con la banca telefónica.\nPara generar o recuperar tu clave y usuario ingresá a la opción Crear clave y usuario, en la pantalla principal de Online Banking, que se encuentra antes de ingresar.\nLa Clave Santander vence 1 vez al año, conocé como modificarla desde {#!data1}.\n\nPara más información sobre claves ingresa {#!data2}.',
    children: [],
    data: [
      {
        text: 'aquí',
        link: 'https://ayuda.santander.com.ar/hc/es-419/articles/360060103754-La-Clave-Santander-y-usuario-tienen-vencimiento',
        type: 'url'
      },
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/articles/360048409533--C%C3%B3mo-genero-mi-Clave-Santander-si-no-la-tengo-o-me-la-olvid%C3%A9',
        type: 'url'
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Online Banking_Motivos->Clave y usuario de acceso',
    intent: 'CLAV_canales_AV'
  },
  {
    id: 'CLAV_pin_credito_AV',
    option_name: 'PIN Tarjeta de Crédito y/o Recargable',
    message: 'Para usar tu tarjeta de crédito y/o recargable en los cajeros automáticos tenés que generar un PIN ingresando a la App Santander en tu celular.\n\nHacelo desde Menú principal » Tarjetas, seleccioná la tarjeta para la que necesites generar el PIN, hace clic en los tres puntitos que se encuentran en el margen superior derecho, elegí “Generar PIN” y seguí los pasos.\nEl PIN se activará luego de 5 minutos de haberlo generado.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Consult_Tarjeta Crédito_Motivos->Consulta por Pin',
    intent: 'CLAV_pin_credito_AV'
  },
  {
    id: 'CTAS_pago_y_debito_servicio_AV',
    option_name: '¿Cómo pago un servicio o impuesto?',
    message: 'Para conocer toda la información sobre pagos y debitos ingresá {#!data1}.\n\nPodés abonar tus servicios haciendo clic {#!data2}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/articles/360047763714--C%C3%B3mo-pago-un-servicio-o-impuesto-por-canales-digitales-',
        type: 'url'
      },
      {
        text: 'acá',
        link: 'gotoNuevoPago()',
        type: 'goto'
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Online Banking_Motivos->Pagos de servicios',
    intent: 'CTAS_pago_y_debito_servicio_AV'
  },
  {
    id: 'CLAV_token_coordenadas_AV',
    option_name: 'Información sobre Token',
    message: 'Para aprobar algunas operaciones en Online Banking o la App necesitás el Token de Seguridad.\nConocé cómo solicitarlo y utilizarlo haciendo clic {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/sections/360009721753-Claves-y-Token',
        type: 'url'
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Online Banking_Motivos->Token/Tarjeta de coordenadas',
    intent: 'CLAV_token_coordenadas_AV'
  },
  {
    id: 'CTAS_migracion_sucursal_AV',
    option_name: 'Quiero cambiar mi cuenta a otra sucursal',
    message: 'Si querés cambiar tu cuenta a otra sucursal puedo derivarte con un asesor que te ayudará de manera personalizada, tené presente que:\nSe te asigna un nuevo número de cuenta, por lo cual tu CBU / Alias también se modifican. (Tendrás que informar los nuevos datos a quienes necesiten realizarte un depósito y/o transferencia).\nSi sos cliente Select, podría cambiar tu ejecutivo Select con quien realizás tus trámites.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Gestión_Cuentas_Motivos->Cambio de radicación de sucursal',
    intent: 'CTAS_migracion_sucursal_AV'
  },
  {
    id: 'CTAS_transferencias_AV',
    option_name: '¿Cómo se realiza una transferencia?',
    message: 'Para conocer toda la información sobre transferencias hacé clic {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/sections/360009417533-Transferencias',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Cuentas_Motivos->Transferencia',
    intent: 'CTAS_transferencias_AV'
  },
  {
    id: 'GRAL_datos_cliente_modificacion_AV',
    option_name: 'Modificar datos personales',
    message: 'Podés modificar tus datos personaliza haciendo clic {#!data1}.\n\nPara poder realizar la modificación es necesario tener activo el Token de Seguridad.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'gotoModificarDatosPersonales()',
        type: 'goto'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Consult_Cuentas_Motivos->Datos personales Carga/modificación',
    intent: 'GRAL_datos_cliente_modificacion_AV'
  },
  {
    id: 'GRAL_baja_producto_AV',
    option_name: 'Dar de baja un producto',
    message: 'Me apena mucho que quieras darte de baja.\nPara solicitarlo, puedo derivarte con un asesor que te ayudará de manera personalizada.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'RETENCION',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Baja de producto',
    intent: 'GRAL_baja_producto_AV'
  },
  {
    id: 'CTAS_resumen_online_AV',
    option_name: '¿Dónde consulto mi resumen?',
    message: 'Podés consultar el resumen online haciendo clic {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'resumen-de-cuenta',
        type: 'stack'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Cuentas_Motivos->Solicitud de resúmenes',
    intent: 'CTAS_resumen_online_AV'
  },
  {
    id: 'GRAL_turnos_online_caja_AV',
    option_name: 'Turno para caja',
    message: 'Conocé toda la información sobre turnos en sucursal ingresando {#!data1}.\nPara solicitar un turno para caja, hacé clic {#!data2}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/sections/360011748493-Turno-Online',
        type: 'url'
      },
      {
        text: 'acá',
        link: 'turno-caja',
        type: 'stack'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Cuentas_Motivos->Turnos en sucursal',
    intent: 'GRAL_turnos_online_caja_AV'
  },
  {
    id: 'GRAL_turnos_online_ejecutivo_AV',
    option_name: 'Turno para ejecutivo',
    message: 'Conocé toda la información sobre turnos en sucursal ingresando {#!data1}.\nPara solicitar un turno con ejecutivo, ingresá {#!data2}.\n¿Tenés dudas o consultas? Puedo derivarte con un asesor que te ayudará de manera personalizada. ',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/sections/360011748493-Turno-Online',
        type: 'url'
      },
      {
        text: 'acá',
        link: 'turno-ejecutivo',
        type: 'stack'
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Consult_Cuentas_Motivos->Turnos en sucursal',
    intent: 'GRAL_turnos_online_ejecutivo_AV'
  },
  {
    id: 'PRES_solicitar_AV',
    option_name: 'Solicitar un Préstamo',
    message: 'Las opciones disponibles para solicitar un préstamo son:\nPrendario: Podés simular y solicitar tu Préstamo ingresando desde {#!data1}.\nPre-Acordado: Activá tu Súper Préstamo Personal desde Menú principal » Consultas » Préstamos.\nPara más información, puedo derivarte con un asesor que te ayudará de manera personalizada.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://www.santander.com.ar/banco/online/personas/creditos/super-prestamos-prendarios',
        type: 'url'
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Adquisición_Prestamos_Motivos->Alta pre acordado',
    intent: 'PRES_solicitar_AV'
  },
  {
    id: 'TARJ_reimpresion_renovacion_AV',
    option_name: 'Reimpresión o renovación',
    message: 'Podés solicitar la reimpresión de tus tarjetas sin costo. Tené en cuenta que, si realizaste la denuncia por robo o extravío, la reimpresión es automática y no tenés que volver a generarla.\nVamos a enviar el plástico nuevo al domicilio que tenés registrado en un lapso de hasta 10 días hábiles. Tu tarjeta actual sigue funcionando hasta que recibas la nueva, cuando esto suceda, asegurate de destruir el plástico anterior.\nPara solicitar la reimpresión hacé clic {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'solicitud-reimpresion-tarjetas',
        type: 'stack'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Gestión_Tarjetas_Motivos->Reimpresión de Plasticos',
    intent: 'TARJ_reimpresion_renovacion_AV'
  },
  {
    id: 'TARJ_pago_AV',
    option_name: 'Pagar tarjeta de crédito',
    message: 'Para realizar el pago de tu tarjeta de crédito desde tu cuenta hacé clic {#!data1}.\nTambién podrás abonar desde Autoservicios, cajeros automáticos de la Red Banelco, PagoMisCuentas y Rapipago.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'pago-tarjeta',
        type: 'stack'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Gestión_Tarjetas_Motivo->Pagos',
    intent: 'TARJ_pago_AV'
  },
  {
    id: 'CTAS_alta_nueva_AV',
    option_name: '¿Cómo abro una cuenta en Santander?',
    message: 'Si necesitas abrir una caja de ahorros ingresá en Menú principal » Solicitudes » Cuentas y paquetes » Solicitar caja de ahorro. Si querés dar de alta una cuenta título, pedila desde Menú principal » Solicitudes » Cuentas y paquetes » Solicitar alta de cuenta título.\nPara más información, llamanos al (011) 4345-2400 o desde el interior sin cargo al 0800-999-2400 y desde el exterior al +54 11 4345-2400, las 24 horas los 365 días del año.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Adquisición_Cuentas_Motivos->a_c_Otros',
    intent: 'CTAS_alta_nueva_AV'
  },
  {
    id: 'CTAS_baja_AV',
    option_name: '¿Cómo cierro mi cuenta en Santander?',
    message: 'Me apena mucho que no quieras tener tu cuenta con nosotros.\nSi querés dar de baja tu cuenta, puedo derivarte con un asesor que te ayudará de manera personalizada',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'RETENCION',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Gestión_Cuentas_Motivos->Solicitud de baja',
    intent: 'CTAS_baja_AV'
  },
  {
    id: 'CTAS_cbu_alias_AV',
    option_name: '¿Qué es CBU y Alias?',
    message: 'La CBU es la clave bancaria uniforme de 22 dígitos que identifica tu cuenta y el Alias es el sobrenombre que se le asigna a esa CBU. A la hora de transferir podés usar uno u otro de forma indistinta y son los mismos tanto para tu cuenta en dólares y/o pesos.\n\nLa CBU de tu cuenta principal es: {#CBUCuentaPrincipal}.\n\nSi también querés conocer y/o modificar tu Alias ingresá a Menú principal » Consultas » Cuentas y hace clic en Consultar Alias y datos de cuenta. Podés modificarlo 1 vez por día y hasta 10 veces en el año calendario.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Gestión_Cuentas_Motivos->Constancia de certificado de cbu',
    intent: 'CTAS_cbu_alias_AV'
  },
  {
    id: 'CTAS_consulta_saldo_movimientos_AV',
    option_name: '¿Dónde puedo verificar los movimientos de mi cuenta?',
    message: 'En la página de inicio podés hacer clic sobre tu cuenta para consultar tu saldo y los últimos movimientos, desde ahí vas a poder ingresar al detalle de los últimos 60 días.\n\nSi necesitás verificar movimientos mayores a 60 días, descargá tu resumen desde Menú principal » Consultas » Cuentas » Consultas y Operaciones » Resúmenes de cuenta.\nPara consultar el saldo y movimientos de tu cuenta hace clic {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'irInicioCuenta',
        type: 'goto'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Consult_Cuentas_Motivos->movimiento de cuentas',
    intent: 'CTAS_consulta_saldo_movimientos_AV'
  },
  {
    id: 'CTAS_transferencia_acreditacion_AV',
    option_name: '¿Cuánto tarda en acreditarse una transferencia?',
    message: 'La acreditación de las transferencias varía según la modalidad que hayas elegido al momento de realizarla:\n- Inmediata: El débito y acreditación se realiza en el acto. En caso de que la realices entre las 23 y 04 horas se visualizarán en movimientos a partir de las 06 horas del día siguiente.\nExisten excepciones en donde por seguridad algunas transferencias inmediatas quedan programadas para realizarse en 48 horas. De todas maneras el dinero se debita de tu cuenta en el momento en que la hacés.\n- Programada: El débito y la acreditación se realiza el día que elijas en el calendario.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Cuentas_Motivos->Transferencia',
    intent: 'CTAS_transferencia_acreditacion_AV'
  },
  {
    id: 'CLAV_clave_fiscal_AV',
    option_name: 'Clave fiscal',
    message: 'Podés generar el blanqueamiento de tu clave fiscal de AFIP desde Menú principal » Consultas » Resumen impositivo » Consultas y Operaciones » Presentar mis DDJJ » Presentar DDJJ.\nPara continuar te va a pedir tu Token y te redirigirá a la web de PagoMisCuentas. En la opción “Servicios AFIP” podrás generar el blanqueo de clave fiscal.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'blanqueo-clave-fiscal',
    intent: 'CLAV_clave_fiscal_AV'
  },
  {
    id: 'CTAS_dudas_por_movimiento_AV',
    option_name: 'Tengo dudas con los movimientos de mi cuenta',
    message: 'Si tenés dudas con algún movimiento de tu cuenta, puedo derivarte con un asesor que te ayudará de manera personalizada',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Consult_Cuentas_Motivos->movimiento de cuentas',
    intent: 'CTAS_dudas_por_movimiento_AV'
  },
  {
    id: 'TARJ_dudas_consumo_AV',
    option_name: 'Tengo dudas con los consumos de mi tarjeta de crédito',
    message: 'Si tenés dudas con algún consumo de tu tarjeta, puedo derivarte con un asesor para que te ayude.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Consult_Tarjeta Crédito_Motivos->Consulta por movimientos/compras',
    intent: 'TARJ_dudas_consumo_AV'
  },
  {
    id: 'GRAL_telefonos_utiles_AV',
    option_name: 'Teléfonos de asistencia del Banco',
    message: 'Podés consultar nuestros teléfonos útiles haciendo clic {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/sections/360009654474-Tel%C3%A9fonos-%C3%BAtiles',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Teléfonos útiles',
    intent: 'GRAL_telefonos_utiles_AV'
  },
  {
    id: 'INVE_compra_venta_moneda_extranjera_AV',
    option_name: 'Comprar / vender dólares',
    message: 'Encontrá más información sobre las condiciones y la documentación de ingresos a presentar para compra de dólares ingresando {#!data1}.\nPodés comprar o vender dólares los días hábiles entre las 10 y las 15 horas haciendo clic {#!data2}.\nLa cotización del día de hoy es {#CotizacionMonedaExtranjeraUSD}\n¿Tenés dudas o consultas sobre dólares? Puedo derivarte con un asesor que te ayudará de manera personalizada.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/sections/360009252074-Moneda-extranjera',
        type: 'url'
      },
      {
        text: 'acá',
        link: 'irInicioDolares',
        type: 'goto'
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Consult_Cuentas_Motivos->Compra/venta de dólares',
    intent: 'INVE_compra_venta_moneda_extranjera_AV'
  },
  {
    id: 'TARJ_autorizacion_compra_AV',
    option_name: '¿Cómo autorizar una compra?',
    message: 'Los motivos por los cuales te pueden pedir autorización son variados. Para consultarlos y autorizar una compra realizada con tu tarjeta Santander tenés que comunicarte por teléfono a estos números:\nMastercard --> Infoline (011) 4348-7000 - Gold Line (011) 4348-7007 - Platinum Line 0800-999-3999 - Black Line 0800-999-3297\nAmerican Express --> (011) 4379-2639 - Gold (011) 4379-2500 - Platinum 0-800-333-0700 solicitando cobro revertido.\nVisa --> (011) 4379-3400 -  0-810-666-3400 - Desde EE.UU 1-877-233-8472 - Desde Brasil 0-800-891-8472 Desde cualquier otra parte del mundo al (54-11) 4379-3434 (solicitá cobro revertido).\nDébito --> (011) 4320-2500',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Tarjeta Crédito_Motivos->Autorización de pagos',
    intent: 'TARJ_autorizacion_compra_AV'
  },
  {
    id: 'TARJ_bloqueo_AV',
    option_name: 'Se bloqueó mi tarjeta',
    message: 'Si se bloqueó tu tarjeta, puedo derivarte con un asesor que te ayudará de manera personalizada.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Problemas_Tarjetas_Motivos->Bloqueos Preventivos',
    intent: 'TARJ_bloqueo_AV'
  },
  {
    id: 'TARJ_debito_servicio_baja_AV',
    option_name: '¿Cómo dar de baja un servicio adherido al débito automático?',
    message: 'Para dar de baja un débito automático adherido a tu cuenta ingresá a Menú principal »Transacciones » Pago de Servicios e Impuestos. Buscá el servicio y presioná el boton de los 3 puntos (más acciones) que está a la derecha de la pantalla y seleccioná Baja de Adhesión.\n\nSi el débito que querés dar de baja se encuentra adherido a tu tarjeta de crédito comunicate con:\nMastercard --> Infoline (011) 4348-7000 - Gold Line (011) 4348-7007 - Platinum Line 0800-999-3999 - Black Line 0800-999-3297\nAmex --> (011) 4379-2639 - Gold (011) 4379-2500 - Platinum.\nVisa --> (011) 4379-3400 -  0-810-666-3400.\nTené en cuenta que la cancelación del débito no implica la baja del servicio.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Tarjeta Crédito_Motivos->Débitos Automáticos',
    intent: 'TARJ_debito_servicio_baja_AV'
  },
  {
    id: 'TARJ_delivery_tarjeta_estado_AV',
    option_name: '¿Cuándo llega mi tarjeta?',
    message: 'El envío de la Tarjeta Santander puede demorar hasta 10 días hábiles y podés hacer el seguimiento del envío ingresando a Menú principal » Solicitudes y Trámites » Seguimiento de Envío de tus Tarjetas. Si estás esperando también el PIN de tu tarjeta de débito, tené presente que se envían de manera separada.\nSi paso el plazo de entrega y aún no la recibiste, puedo derivarte con un asesor que te ayudará de manera personalizada.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: 'deliveryTarjeta',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Problemas_Tarjetas_Motivos->Delivery de tarjetas',
    intent: 'TARJ_delivery_tarjeta_estado_AV'
  },
  {
    id: 'TARJ_denuncia_extravio_o_robo_AV',
    option_name: 'Quiero denunciar robo o extravío de tarjeta',
    message: 'Conocé cómo realizar la denuncia de tu tarjeta de débito, crédito y/o recargable haciendo clic {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/articles/1500002711281--C%C3%B3mo-denuncio-el-robo-o-la-p%C3%A9rdida-de-mi-tarjeta-de-cr%C3%A9dito-o-mi-tarjeta-recargable-desde-los-canales-digitales-',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Denuncia robo o extravío de tarjeta',
    intent: 'TARJ_denuncia_extravio_o_robo_AV'
  },
  {
    id: 'TARJ_extraccion_por_cajero_AV',
    option_name: '¿Cómo hacer una extracción por cajero sin tarjeta de débito?',
    message: 'Para extraer dinero desde un cajero Banelco sin usar la tarjeta de débito, podés hacer una extracción sin tarjeta hasta $10.000. Solicitalo desde Menú principal » Transacciones » Extracción sin tarjeta.\nConocé más informacion ingresando {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/articles/360048409853--C%C3%B3mo-hago-extracci%C3%B3n-sin-tarjeta-',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Extraccion sin tarjeta',
    intent: 'TARJ_extraccion_por_cajero_AV'
  },
  {
    id: 'TARJ_habilitacion_AV',
    option_name: '¿Cómo habilitar mi tarjeta?',
    message: 'La tarjeta de débito ya está habilitada para que puedas utilizarla. Si necesitás la clave para operar por cajero automático, generala ingresando {#!data1}.\nPara habilitar la tarjeta de crédito comunicate al teléfono que se encuentra al dorso del plástico.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'perfil-blanqueo-clave-banelco',
        type: 'stack'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Gestión_Tarjetas_Motivos->Habilitación de tarjetas',
    intent: 'TARJ_habilitacion_AV'
  },
  {
    id: 'TARJ_marca_viajero_AV',
    option_name: 'Dar aviso de viaje para usar mis tarjetas en el exterior',
    message: 'Para dar aviso de viaje de tus tarjetas Visa y American Express, puedo derivarte con un asesor que te ayudará de manera personalizada.\nPara dar aviso de viaje de tu tarjeta MasterCard llamá al (011) 4379-7763.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Problemas_Tarjetas_Motivos->Aviso de viaje',
    intent: 'TARJ_marca_viajero_AV'
  },
  {
    id: 'TARJ_refinanciacion_AV',
    option_name: 'Financiación de tarjeta de crédito',
    message: 'Puedo ayudarte con la siguiente información sobre financiación de tarjeta de crédito:\nVisa y American Express ->  primero tenés que abonar el pago mínimo y luego solicitarla desde la opción Tarjetas » Consultas y Operaciones » Financiación de resumen.\nMasterCard -> En el resumen de tu tarjeta vas a encontrar opciones de planes en cuotas para financiar tus consumos. Para solicitarlo, aboná el monto exacto de la cuota del plan que más te convenga. Ese pago cancela el pago mínimo. También podés realizar la financiación abonando el pago mínimo y llamando al (011) 4379-7763 opción 6.\nRefinanción tarjeta de crédito dispuesta por el BCRA -> conocé toda la información haciendo clic {#!data1}.\nCancelar la financiación -> Si querés pagar de una sola vez todas tus cuotas pendientes tenés que hacer una caída de plan de cuotas. Para solicitarlo, puedo derivarte con un asesor que te ayude de manera personalizada.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/articles/360048411953--Tengo-la-posibilidad-de-refinanciar-el-saldo-a-pagar-mis-Tarjetas-de-Cr%C3%A9dito-Santander',
        type: 'url'
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Consult_Tarjeta Crédito_Motivos->Refinanciación',
    intent: 'TARJ_refinanciacion_AV'
  },
  {
    id: 'TARJ_tc_baja_AV',
    option_name: 'Quiero dar de baja una tarjeta',
    message: 'Me apena mucho que no quieras tener tu tarjeta con nosotros.\nPara darla de baja, hace clic {#!data1} y seleccioná Consultas y Operaciones. Allí vas a poder dar de baja tarjeta titular y/o adicional.\n\n¡Estaré acá siempre que quieras volver a vivir la experiencia Santander!',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'irInicioTarjetasSolicitudes',
        type: 'goto'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Bajas_Tarjetas_Motivos->Otros',
    intent: 'TARJ_tc_baja_AV'
  },
  {
    id: 'CTAS_descubierto_AV',
    option_name: '¿Cómo funciona el descubierto?',
    message: 'El Banco tiene un límite de descubierto disponible según el tipo de cuenta que poseas. Esto significa que cuando el saldo de tu cuenta esté llegando a 0 (cero), y necesites hacer pagos, transferencias, compras con tarjeta de débito, extracciones o emitir cheques, podrás hacerlo usando el descubierto hasta ese límite, de manera automática. Te muestro un ejemplo:\nTenés un saldo de $500 en tu cuenta y te ingresa un débito automático de un servicio por $700. Tu cuenta queda con un saldo deudor de - $200.\nPara cancelar este saldo negativo (-$200) tenés que hacer un depósito por el monto adeudado.\n\nCuando tu cuenta esté en descubierto, el saldo aparecerá con un signo menos adelante. Consultá tu límite desde Menú Principal » Consultas » Cuentas.\nTené en cuenta que los movimientos que realices en tu Cuenta Corriente tributan bajo la Ley 25.413.\nSi utilizás el descubierto, se cobrará una tasa de interés diario, que podés consultar en tu resumen de cuenta. El cobro del interés se realiza a mes vencido, el día 6 o hábil siguiente.\nSi no tenés disponible límite de descubierto en tu cuenta y/o querés modificarlo, tenés que solicitarlo en tu sucursal. Para solicitar un turno, ingresá {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'turno-ejecutivo',
        type: 'stack'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Consult_Cuentas_Motivos->Consulta de saldos',
    intent: 'CTAS_descubierto_AV'
  },
  {
    id: 'CTAS_echeq_AV',
    option_name: '¿Cómo operar con Echeq?',
    message: 'Desde Online Banking vas a poder operar con Echeq haciendo clic {#!data1}.\n\nConocé toda la información sobre Echeq ingresando {#!data2}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'irInicioECheq',
        type: 'goto'
      },
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/sections/360010453554-ECHEQ',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Consult_Cuentas_Motivos->Cheques',
    intent: 'CTAS_echeq_AV'
  },
  {
    id: 'CTAS_transferencia_inconveniente_AV',
    option_name: 'Tengo un inconveniente con mi transferencia',
    message: 'Si intentás realizar una transferencia y durante el proceso recibís un mensaje de error en la comunicación tené presente lo siguiente:\n\nVerifica que el número de cuenta o CBU (consta de 22 dígitos) a donde querés transferir sean correctos.\nQue la cuenta destino de los fondos esté activa.\nTener Token de Seguridad para aprobar la transferencia.\nSi denunciaste tu tarjeta de débito por perdida o extravío, será necesario que aguardes a que se emita el nuevo plástico y comience el proceso de envío para poder transferir nuevamente. No es necesiario que generes un nuevo Token.\nPor seguridad, algunas transferencias inmediatas se agendan como programadas para 24 o 48 horas (aunque se trate de un destinatario recurrente).\nSi se rechaza por la entidad de destino, la devolución del dinero se realiza en un plazo de 24/48 horas hábiles.\nSi querés transferir a una cuenta judicial o fiscal, deberás hacerlo por sucursal.\nPara transferir dólares, corroborá con el destinatario que tenga cuenta habilitada para recibir moneda extranjera y cuando la agendes, seleccioná la moneda dólares.\nSi el inconveniente persiste, puedo derivarte con un asesor para que te ayude de manera personalizada.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Problemas_Cuentas_Motivos->Incidencia con transferencia',
    intent: 'CTAS_transferencia_inconveniente_AV'
  },
  {
    id: 'CTAS_upgrade_AV',
    option_name: 'Quiero hacer un upgrade de cuenta',
    message: 'Si querés solicitar un upgrade de cuenta, llamanos al (011) 4345-2400 o desde el interior sin cargo al 0800-999-2400 y desde el exterior al +54 11 4345-2400, las 24 horas los 365 días del año.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Adquisición_Cuentas_Motivos->Upgrade',
    intent: 'CTAS_upgrade_AV'
  },
  {
    id: 'CTAS_comisiones_AV',
    option_name: '¿Cuáles son las comisiones de mi cuenta?',
    message: 'Para conocer toda la información sobre costos, comisiones de tu cuenta y otros productos del Banco, ingresá en el siguiente {#!data1}.',
    children: [],
    data: [
      {
        text: 'link',
        link: 'https://www.santander.com.ar/banco/online/personas/acerca-de-nosotros/legales/comisiones-BCRA',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Cuentas_Motivos->Comisiones',
    intent: 'CTAS_comisiones_AV'
  },
  {
    id: 'TARJ_comisiones_AV',
    option_name: '¿Cuáles son las comisiones de mi tarjeta?',
    message: 'Para conocer toda la información sobre costos, comisiones de tu tarjeta, ingresá en el siguiente {#!data1}.',
    children: [],
    data: [
      {
        text: 'link',
        link: 'https://www.santander.com.ar/banco/online/personas/acerca-de-nosotros/legales/comisiones-BCRA',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Cuentas_Motivos->Comisiones',
    intent: 'TARJ_comisiones_AV'
  },
  {
    id: 'CTAS_comprobantes_AV',
    option_name: 'Quiero consultar mis comprobantes',
    message: 'Podés consultar los comprobantes de tus operaciones haciendo clic {#!data1}.\nUtilizá el buscador para descargar comprobantes más antiguos. Te recomiendo que siempre descargues los comprobantes cuando finalices una operación.\nTambién podés consultar tus transacciones en el resumen de cuenta o en los últimos movimientos de tu cuenta.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'irInicioComprobantes',
        type: 'goto'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Comprobantes',
    intent: 'CTAS_comprobantes_AV'
  },
  {
    id: 'CTAS_deposito_acreditacion_AV',
    option_name: '¿Cuánto tarda en acreditarse un depósito?',
    message: 'El tiempo de acreditación de un depósito varía según la forma en la que lo hayas realizado:\nDepósito en efectivo en un Autoservicio sin sobre: La acreditación es automática.\nDepósito en efectivo en un Autoservicio con sobre: La acreditación se efectiviza en el día hábil siguiente, a partir de las 16 horas.\nDepósitos de cheques: Si el cheque a depositar es de Santander, se acredita dentro de las 24 horas hábiles. Si el cheque es de otro banco, la acreditación se verá reflejada a las 72 horas hábiles.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Acreditación depósito',
    intent: 'CTAS_deposito_acreditacion_AV'
  },
  {
    id: 'TARJ_desconocimiento_consumo_AV',
    option_name: 'En mi tarjeta aparece un consumo que no realicé',
    message: 'Si tenés dudas sobre un consumo realizado con tu tarjeta de crédito y/o débito ingresá {#!data1}.\nSi querés realizar el desconocimiento del consumo, puedo derivarte con un asesor que te ayudará de manera personalizada.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/sections/360009660433-Desconocimiento',
        type: 'url'
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Consult_Tarjeta Crédito_Motivos->Desconocimiento de compra',
    intent: 'TARJ_desconocimiento_consumo_AV'
  },
  {
    id: 'CTAS_mi_ejecutivo_de_cuenta_AV',
    option_name: '¿Quién es mi ejecutivo de cuenta?',
    message: 'Podés conocer quien es tu ejecutivo dentro de la opción Mi Perfil, en el recuadro Tu Ejecutivo.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: 'miEjecutivo',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Mi ejecutivo',
    intent: 'CTAS_mi_ejecutivo_de_cuenta_AV'
  },
  {
    id: 'CTAS_pago_debito_inconveniente_AV',
    option_name: 'Tengo un inconveniente con el pago de un servicio / impuesto',
    message: 'Si tenés algún inconveniente con un pago y/o débito automático de un servicio / impuesto, ingresá a nuestro Centro de Ayuda y conocé las recomendaciones a tener en cuenta {#!data1}.',
    children: [],
    data: [
      {
        text: 'aquí',
        link: 'https://ayuda.santander.com.ar/hc/es-419/articles/360054955974-Recomendaciones-a-tener-en-cuenta-para-pagar-servicios-a-trav%C3%A9s-de-Online-Banking-y-APP-Santander',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Problemas_Cuentas_Motivos->Pagos',
    intent: 'CTAS_pago_debito_inconveniente_AV'
  },
  {
    id: 'GRAL_reclamo_estado_AV',
    option_name: 'Tengo un reclamo y quiero conocer su estado',
    message: 'Para conocer el estado de tu reclamo, puedo derivarte con un asesor que te ayudará de manera personalizada.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Consult_Cuentas_Motivos->seguimiento gestión de reclamo',
    intent: 'GRAL_reclamo_estado_AV'
  },
  {
    id: 'GRAL_reclamo_promocion_AV',
    option_name: 'Quiero hacer un reclamo por una promoción no realizada',
    message: 'Si realizaste una compra que tenía una promoción y no se acreditó, tené presente lo siguiente:\nPor compras realizadas con tarjeta de debito, el reintegro se realiza a los 15 hábiles.\nEn compras tarjeta de crédito, se efectúa dentro de los 30 días hábiles, esto quiere decir que puede aparecer en la liquidación actual o en la próxima.\nSi se cumplió el plazo y aún no verificás el reintegro, puedo derivarte con un asesor que te ayudará de manera personalizada.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Problemas_Tarjetas_Motivos->Promociones',
    intent: 'GRAL_reclamo_promocion_AV'
  },
  {
    id: 'GRAL_reclamo_comision_AV',
    option_name: 'Quiero hacer un reclamo por comisiones',
    message: 'Si tenés dudas o querés hacer un reclamo sobre las comisiones cobradas en tu cuenta, puedo derivarte con un asesor que te ayudará de manera personalizada.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Gestión_Tarjetas_Motivos->Devolución de comisiones en tarjetas y en cuenta, demora gestión 3 dias',
    intent: 'GRAL_reclamo_comision_AV'
  },
  {
    id: 'GRAL_reclamo_otro_problema_AV',
    option_name: 'Tengo otro problema con el Banco',
    message: 'Para resolver tu inconveniente, puedo derivarte con un asesor que te ayudará de manera personalizada.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Problema con el banco',
    intent: 'GRAL_reclamo_otro_problema_AV'
  },
  {
    id: 'GRAL_reclamo_producto_AV',
    option_name: 'Tengo dado de alta un producto / servicio del banco que no autorice',
    message: 'Para resolver puntualmente esta consulta, puedo derivarte con un asesor que te ayudará de manera personalizada.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Baja de producto',
    intent: 'GRAL_reclamo_producto_AV'
  },
  {
    id: 'INVE_plazo_fijo_alta_AV',
    option_name: '¿Cómo hago un plazo fijo?',
    message: 'Podés constituir plazos fijos todos los días hábiles entre las 7.30 a 21 horas haciendo clic {#!data1}.\nCuando simules el plazo fijo, podrás conocer los intereses a cobrar al vencimiento del plazo fijo, en función del monto que ingreses.\nConocé más información sobre plazos fijos ingresando {#!data2}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'gotoConstitucionPlazoFijo()',
        type: 'goto'
      },
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/sections/360009241934-Plazo-fijo',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Adquisición_Inversiones_Motivos->Plazo fijo',
    intent: 'INVE_plazo_fijo_alta_AV'
  },
  {
    id: 'INVE_informacion_AV',
    option_name: 'Más información sobre inversiones',
    message: 'Conocé toda la información sobre inversiones ingresando {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/categories/360004085333-Inversiones',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Consulta_Inversiones_Motivos->Asesoramiento',
    intent: 'INVE_informacion_AV'
  },
  {
    id: 'PRES_precancelacion_AV',
    option_name: '¿Puedo cancelar un préstamo?',
    message: 'Para conocer información sobre cómo cancelar préstamos hace clic acá:\nPréstamo prendario {#!data1}.\nPréstamo hipotecario {#!data2}.\nPréstamo personal {#!data3}.',
    children: [],
    data: [
      {
        text: 'aquí',
        link: 'https://ayuda.santander.com.ar/hc/es-419/articles/360051604274--Puedo-hacer-una-cancelaci%C3%B3n-anticipada-de-mi-Pr%C3%A9stamo-Prendario-En-qu%C3%A9-momento-',
        type: 'url'
      },
      {
        text: 'aquí',
        link: 'https://ayuda.santander.com.ar/hc/es-419/articles/360060743533--Puedo-precancelar-el-pr%C3%A9stamo-o-adelantar-cuotas-',
        type: 'url'
      },
      {
        text: 'aquí',
        link: 'https://ayuda.santander.com.ar/hc/es-419/articles/360051961953--Puedo-hacer-una-cancelaci%C3%B3n-anticipada-de-mi-pr%C3%A9stamo-personal-En-qu%C3%A9-momento-',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Gestión_Prestamos_Motivos->Otros',
    intent: 'PRES_precancelacion_AV'
  },
  {
    id: 'PRES_valor_tasa_AV',
    option_name: '¿Cuáles son las tasas e intereses del préstamo?',
    message: 'Para conocer las tasas e interés de los préstamos, hacé clic {#!data1}.',
    children: [],
    data: [
      {
        text: 'aquí',
        link: 'https://www.santander.com.ar/banco/online/personas/acerca-de-nosotros/legales/comisiones-BCRA',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Consulta_Prestamos_Motivos->Tasas',
    intent: 'PRES_valor_tasa_AV'
  },
  {
    id: 'TARJ_acreditacion_pago_AV',
    option_name: 'No se acreditó el pago de mi tarjeta',
    message: 'Si hiciste un pago y no lo ves detallado, tené en cuenta que la acreditación puede demorar hasta 72 horas. Una vez acreditado, se liberará tu límite disponible para que puedas utilizarlo. Si necesitás descargar comprobantes por los pagos realizados, hacelo desde Menú principal » Consultas » Comprobantes » Transacciones.\n¿Tenés dudas o consultas sobre el pago? Puedo derivarte con un asesor que te ayudará de manera personalizada.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Problemas_Tarjetas_Motivos->Pagos',
    intent: 'TARJ_acreditacion_pago_AV'
  },
  {
    id: 'TARJ_envio_de_efectivo_AV',
    option_name: '¿Cómo realizo un envío de efectivo?',
    message: 'Para extraer hasta $10.000 desde un cajero Banelco sin usar la tarjeta de débito podés hacer:\nExtracción sin tarjeta: si vos vas a ir al cajero a hacer la extracción. Para solicitarlo clic {#!data1}.\nEnvío de Efectivo: si un tercero se va a acercar al cajero. Está opción solo está disponible en la App.\nPara poder realizar esta operación es necesario tener activo el Token de Seguridad.\nConocé más información ingresando {#!data2}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'gotoExtraccionSinTarjeta()',
        type: 'goto'
      },
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/articles/360048409853--C%C3%B3mo-hago-extracci%C3%B3n-sin-tarjeta-',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Envío de efectivo',
    intent: 'TARJ_envio_de_efectivo_AV'
  },
  {
    id: 'TARJ_extraccion_inconveniente_AV',
    option_name: 'Extracción en un cajero',
    message: 'Si tuviste algún inconveniente durante una extracción de dinero por Cajero Automático o Autoservicio puedo derivarte con un asesor para que te ayude.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Extracciones no dispensadas',
    intent: 'TARJ_extraccion_inconveniente_AV'
  },
  {
    id: 'TARJ_limites_disponibles_ampliacion_AV',
    option_name: 'Quiero ampliar el límite de mi tarjeta de crédito',
    message: 'Si querés solicitar una ampliación del límite de compra de tu tarjeta de crédito, tenés que acercarte a tu sucursal, con tu DNI y documentación respaldatoria de tus ingresos.Según tu situación laboral, tendrás que llevar los tres últimos recibos de sueldo, tu comprobante de monotributo o DDJJ de Ganancias. La ampliación quedará sujeta a análisis. Para solicitar un turno con ejecutivo hacé clic {#!data1}.\nTambién tenés la opción de pedir una ampliación transitoria para compra puntual en tu sucursal, sólo con tu DNI. Tené en cuenta que la ampliación equivale al 30% de tu límite de compra al contado y tiene una vigencia de 30 días.\nPara el caso de las compras con tarjeta de débito, todos los clientes tienen un límite fijo de $10.000 diarios.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'turno-ejecutivo',
        type: 'stack'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Gestión_Tarjetas_Motivos->Recalificación de limites',
    intent: 'TARJ_limites_disponibles_ampliacion_AV'
  },
  {
    id: 'TARJ_limites_disponibles_consulta_AV',
    option_name: 'Quiero saber los límites de mi tarjeta',
    message: 'Si sos titular, podés consultar los límites y disponibles de tus Tarjetas Santander Crédito desde Online Banking. Te explico cómo:\n\n1️⃣ Abrí el Menú, buscá la sección “Consultas” y seleccioná “Tarjetas”.\n2️⃣ En el buscador, elegí la tarjeta a consultar.\n3️⃣ Abrí la barra de “Consultas y operaciones” y seleccioná “Límites y disponibles”.\n\nTené en cuenta que los disponibles se actualizan a partir de la fecha de cierre y al ingresar los pagos. Pueden demorar hasta 72 horas hábiles.\n\nPara la tarjeta de débito, podés consultar o modificar los límites de extracción, haciendo clic {#!data1}.\n\nSi tenés dudas o consultas, puedo derivarte con un asesor para que te ayude.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'gotoSolicitudCambioLimiteExtraccion()',
        type: 'goto'
      }
    ],
    integration: 'limites',
    derivation: 'GENERAL',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Tarjeta Crédito_Motivos->Consulta de saldos',
    intent: 'TARJ_limites_disponibles_consulta_AV'
  },
  {
    id: 'TARJ_promocion_no_realizada_AV',
    option_name: 'No veo aplicada una promoción',
    message: 'Si realizaste una compra que tenía una promoción y no se acreditó, tené presente lo siguiente:\nPor compras realizadas con tarjeta de debito, el reintegro se realiza a los 15 hábiles.\nEn compras tarjeta de crédito, se efectúa dentro de los 30 días hábiles, esto quiere decir que puede aparecer en la liquidación actual o en la próxima.\nSi se cumplió el plazo y aún no verificás el reintegro, puedo derivarte con un asesor que te ayudará de manera personalizada.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Problemas_Tarjetas_Motivos->Promociones',
    intent: 'TARJ_promocion_no_realizada_AV'
  },
  {
    id: 'TARJ_promociones_vigentes_AV',
    option_name: '¿Cuáles son las promociones vigentes?',
    message: 'Encontrá toda la información que necesitás sobre las promociones y beneficios de tus tarjetas Santander (ahorros, reintegros, locales adheridos y las tarjetas que aplican) {#!data1}. \nSi tenés dudas sobre una promoción que recibiste por mail, en el mismo correo vas a encontrar toda la información que necesitás.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://www.santander.com.ar/banco/online/personas/beneficios',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Consult_Tarjeta Crédito_Motivos->Promociones',
    intent: 'TARJ_promociones_vigentes_AV'
  },
  {
    id: 'TARJ_sorpresa_alta_o_baja_AV',
    option_name: 'Sorpresa Santander',
    message: 'Con Sorpresa Santander accedés a ahorros exclusivos de hasta 35% en las mejores marcas. El alta la puede solicitar tanto el titular de la cuenta/tarjeta como alguno de sus firmantes/adicionales y la adhesión es inmediata. La comisión del servicio es de $72 (IVA incluido) y aparecerá siempre en el resumen del titular con la leyenda “COM SORPRESA SANTANDER RIO”.\nPodés dar de alta Sorpresa haciendo clic {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'gotoMyASuscripcionSorpresa()',
        type: 'goto'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Tarjeta Crédito_Motivos->Sorpresa',
    intent: 'TARJ_sorpresa_alta_o_baja_AV'
  },
  {
    id: 'TARJ_tc_adicional_alta_AV',
    option_name: 'Adicional de tarjeta de crédito',
    message: 'Podés darle una adicional de tu Tarjeta Santander Crédito sin cargo a cualquier persona mayor de 14 años.\nPara solicitarla tené a mano estos datos:\n- DNI.\n- Fecha de nacimiento.\n- CUIL (en caso de que tenga entre 14 y 18 años).\nLas adicionales comparten el límite de compra con vos y podés ponerles un tope de gastos mensuales. Las tarjetas te llegarán dentro de los 10 días hábiles al domicilio que tenés registrado. Para solicitarla, hace clic {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'gotoSolicitudTarjetaAdicional()',
        type: 'goto'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Gestión_Tarjetas_Motivos->Alta adicionales',
    intent: 'TARJ_tc_adicional_alta_AV'
  },
  {
    id: 'TARJ_tc_alta_nueva_AV',
    option_name: 'Solicitar una tarjeta de crédito',
    message: 'Si querés adquirir alguna de las Tarjetas Santander Crédito, ingresa en www.santander.com.ar para conocer las opciones que tenemos disponibles para vos. Para solicitar las tarjetas Visa, American Express y/o MasterCard, deberás acercarte a tu sucursal. Para solicitar un turno con ejecutivo ingresá {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'turno-ejecutivo',
        type: 'stack'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Adquisición_Tarjetas_Motivos->Alta mono producto',
    intent: 'TARJ_tc_alta_nueva_AV'
  },
  {
    id: 'TARJ_tc_fecha_vencimiento_cierre_cambio_AV',
    option_name: 'Fecha de vencimiento',
    message: 'Podés consultar la fecha de cierre y vencimiento del resumen de tu tarjeta de crédito ingresando desde Menú principal » Consultas » Tarjetas.\nDesde ahí también podés revisar el saldo a pagar en pesos / dólares y el pago mínimo de la liquidación actual. Tené en cuenta que los consumos que hagas el día del cierre deberían ingresar en el resumen en curso.\nSi querés modificar la fecha de vencimiento o cierre de tus tarjetas de crédito, puedo derivarte con un asesor que te ayudará de manera personalizada.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'GENERAL',
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'Consult_Tarjeta Crédito_Motivos->Consulta de saldos',
    intent: 'TARJ_tc_fecha_vencimiento_cierre_cambio_AV'
  },
  {
    id: 'TARJ_tc_stop_debit_AV',
    option_name: 'Stop Debit',
    message: 'El Stop Debit suspende por única vez el pago de tu tarjeta por débito automático. Esta acción no es una baja definitiva, por lo tanto, el resumen siguiente volverá a debitarse normalmente de tu cuenta.\nConocé como y cuando solicitarlo ingresando {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/articles/360047763994--C%C3%B3mo-hago-el-Stop-Debit-de-mis-tarjetas-',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Tarjeta Crédito_Motivos->Stop Debit',
    intent: 'TARJ_tc_stop_debit_AV'
  },
  {
    id: 'TARJ_ultimos_consumos_AV',
    option_name: 'Consultar mis últimos consumos y resumen',
    message: 'Para consultar los últimos movimientos de tu tarjeta de crédito hacé clic {#!data1}.\nPodrás consultar los últimos consumos y el resumen de la tarjeta que hayas definido como principal. Si querés consultar los movimientos de otra tarjeta, deberás seleccionarla desde Cambiar Tarjeta en el menú selector.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'irInicioTarjetas',
        type: 'goto'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Consult_Tarjeta Crédito_Motivos->Consulta de saldos',
    intent: 'TARJ_ultimos_consumos_AV'
  },
  {
    id: 'GRAL_superclub_como_funciona_AV',
    option_name: 'Información sobre SuperClub',
    message: 'Conocé toda la información sobre SuperClub ingresando {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/sections/360009255414-SuperClub',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Tarjeta Crédito_Motivos->SuperClub',
    intent: 'GRAL_superclub_como_funciona_AV'
  },
  {
    id: 'CTAS_cheques_AV',
    option_name: 'Información sobre cheques',
    message: 'Un cheque es un documento de pago que opera siempre en cuenta corriente y sólo en pesos. Para cobrarlo, podés depositarlo en los cajeros y autoservicios de cualquier sucursal Santander. Si querés cobrarlo por ventanilla tenés que acercarte a la sucursal donde fue emitido el cheque. En cualquiera de los dos casos el cheque siempre debe estar endosado. En caso de que quieras transferir un cheque a otra persona también debes endosarlo.\nPara endosar un cheque debes firmarlo en el dorso y colocar tu nombre y apellido completo, número de documento y domicilio, (si lo vas a depositar en una cuenta, debes agregar también el tipo y número de cuenta). Los cheques comunes pueden tener solo un endoso y los cheques de pago diferido hasta dos.\nConocé más información sobre cheques ingresando {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/sections/360009256534-Cheques',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Tarjeta Crédito_Motivos->SuperClub',
    intent: 'CTAS_cheques_AV'
  },
  {
    id: 'PRES_pago_de_cuota_AV',
    option_name: '¿Cómo pago las cuotas del préstamo?',
    message: 'Desde Menú principal » Consultas » Préstamos podés consultar el historial de pago de tu Súper Préstamo, el saldo de tu deuda y el detalle de la próxima cuota por vencer.\nPara más información ingresa {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/categories/360004228434-Pr%C3%A9stamos',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Gestión_Prestamos_Motivos->Pago cuota',
    intent: 'PRES_pago_de_cuota_AV'
  },
  {
    id: 'CTAS_comex_transferencia_AV',
    option_name: 'Información sobre transferencias internacionales',
    message: 'Conocé toda la información sobre transferencias internacionales haciendo clic {#!data1}.',
    children: [],
    data: [
      {
        text: 'acá',
        link: 'https://ayuda.santander.com.ar/hc/es-419/categories/360004242754-Cobros-y-Pagos-Internacionales',
        type: 'url'
      }
    ],
    integration: '',
    derivation: '',
    csat: {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'Consult_Comex_Motivos->Transferencia realizadas',
    intent: 'CTAS_comex_transferencia_AV'
  },
  {
    id: 'MORA_temprana_AV',
    option_name: '',
    message: '{#NombreCliente}, para conocer más información sobre tus productos pendientes de pago, puedo derivarte con un asesor para que te ayude.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: 'moraTemprana',
    derivation: 'MORA',
    csat: {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'MORA',
    intent: 'MORA_temprana_AV'
  },
  {
    id: 'MORA_doble_AV',
    option_name: '',
    message: '{#NombreCliente}, para conocer más información sobre tus productos pendientes de pago, puedo derivarte con un asesor para que te ayude.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: 'MORA',
    csat: {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: 'MORA',
    intent: 'MORA_doble_AV'
  },
  {
    id: 'MORA_tardia_AV',
    option_name: '',
    message: '{#NombreCliente}, para conocer más información sobre tus productos pendientes de pago, puedo derivarte con un asesor para que te ayude.',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: 'moraTardia',
    derivation: 'MORA',
    csat: {
      show: true,
      derivate_when_negative: false
    },
    contact_reason: 'MORA',
    intent: 'MORA_tardia_AV'
  },
  {
    id: 'CHIT_error_deriva_AV',
    option_name: '',
    message: 'Nuestro último operador disponible acaba de ocuparse. Por favor, escribime de nuevo en unos minutos e intentaré volver a derivarte.',
    children: [
      'CLAV_menu_AV',
      'CTAS_menu_AV',
      'TARJ_menu_AV',
      'PRES_menu_AV',
      'GRAL_menu_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: '',
    csat: {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'CHIT_error_deriva_AV'
  },
  {
    id: 'CTAS_resumen_mas18_meses_AV',
    option_name: 'No, es anterior',
    message: 'Aqui podras obtener tus Resumenes anteriores. 👇',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Cuentas_Motivos->Solicitud de resúmenes',
    intent: 'CTAS_resumen_mas18_meses_AV'
  },
  {
    id: 'TARJ_resumen_mas18_meses_AV',
    option_name: 'No, es anterior',
    message: 'Aqui podras obtener tus Resumenes anteriores. 👇',
    children: [],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: '',
    csat:
    {
      show: true,
      derivate_when_negative: true
    },
    contact_reason: 'Consult_Cuentas_Motivos->Solicitud de resúmenes',
    intent: 'TARJ_resumen_mas18_meses_AV'
  },
  {
    id: 'TARJ_menu_resumen_AV',
    option_name: 'Consultar mis últimos consumos y resumen',
    message: '¿El resumen que necesitás es del último año y medio? ',
    children: [
      'TARJ_ultimos_consumos_AV',
      'TARJ_resumen_mas18_meses_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'TARJ_menu_resumen_AV'
  },
  {
    id: 'CTAS_menu_resumen_AV',
    option_name: '¿Dónde consulto mi resumen?',
    message: '¿El resumen que necesitás es de los últimos dos años? ',
    children: [
      'CTAS_resumen_online_AV',
      'CTAS_resumen_mas18_meses_AV'
    ],
    data: [
      {
        text: '',
        link: '',
        type: ''
      }
    ],
    integration: '',
    derivation: false,
    csat:
    {
      show: false,
      derivate_when_negative: false
    },
    contact_reason: '',
    intent: 'CTAS_menu_resumen_AV'
  }
]
