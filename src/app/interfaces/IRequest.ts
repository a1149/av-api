
interface Input {
  type: 'text' | 'option'
  value: string
}

export enum nameChannels { TBAN = 'TBAN', APPM = 'APPM' }

export interface IRequestNode {
  sessionId: string
  input: Input
  channel: nameChannels
}
