/* eslint-disable camelcase */

import { ICsat, IDataTag } from '@app/interfaces/ITreeNode'

export interface IResponse {
  code: string,
  statusCode: number,
  message: string,
  result?: any
}

export interface IOptions {
  id: string,
  text: string,
}
export interface IDataNodeResponse {
  text: string,
  options: IOptions[],
  intent: string,
  csat: ICsat,
  derivation: string | boolean,
  data: IDataTag[],
  integration: string
}

export interface IResponseNode extends IResponse {
  result: IDataNodeResponse
}
