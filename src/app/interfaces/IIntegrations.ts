export interface IOptions {
  id: string,
  text: string,
}

export interface IIntegrationRequest {
  sessionId: string,
  integration: string,
  nup: number,
  channel: 'TBAN' | 'APPM',
  option: string
}

export interface IIntegrationResponse {
  text: string,
  options: IOptions[] | null,
  derivate: boolean,
  activeSecuentialDialogue: boolean
}
