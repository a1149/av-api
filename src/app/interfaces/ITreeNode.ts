/* eslint-disable camelcase */

export interface ICsat {
  show: string | boolean,
  derivate_when_negative: string | boolean
}

export interface IDataTag {
  text: string,
  link: string,
  type: 'url' | 'stack' | 'goto'
}

export interface INode {
  id: string,
  option_name: string,
  message: string,
  children: string[],
  data: IDataTag[],
  derivation: string | boolean,
  integration: string,
  csat: ICsat,
  contact_reason: string,
  intent: string
}

export interface ITree {
  INode: []
}
