import { IDataNodeResponse, INode, IRequestNode } from '@app/interfaces'
import { getIntegrationService } from '@root/app/services'
import { IIntegrationRequest, IIntegrationResponse } from '@root/app/interfaces/IIntegrations'
import { cache } from '@root/database'
import { CACHE_ACTIVE_INTEGRATION_NODE, IS_DEFAULT_RESPONSE } from '@root/app/constants'
import { logger } from 'av-core-express/lib/handlers'

export class Integration {
  private sessionId
  private channel
  private nup
  private node
  private option

  constructor(body: IRequestNode, node: INode, nup: number) {
    this.node = node
    this.channel = body.channel
    this.nup = nup
    this.sessionId = body.sessionId
    this.option = body.input.value
  }

  private default(): IDataNodeResponse {
    const defautl: IDataNodeResponse = {
      text: this.node.message,
      options: [],
      intent: this.node.intent,
      csat: this.node.csat,
      derivation: this.node.derivation,
      data: this.node.data,
      integration: this.node.integration
    }
    return defautl
  }

  async get(): Promise<IDataNodeResponse> {
    try {
      const integrationRequest: IIntegrationRequest = {
        sessionId: this.sessionId,
        nup: this.nup,
        channel: this.channel,
        integration: this.node.integration,
        option: this.option
      }

      const context = await cache.getContext(this.nup)

      if (!context) return this.default()

      const integrationResponse = await getIntegrationService(integrationRequest)

      if (integrationResponse.code === IS_DEFAULT_RESPONSE) return this.default()

      const integration: IIntegrationResponse = integrationResponse.result

      const isActiveIntegrationSecuential = integration.activeSecuentialDialogue

      let csat = this.node.csat
      const derivation = integration.derivate ? this.node.derivation : false
      if (isActiveIntegrationSecuential) {
        csat = {
          show: false,
          derivate_when_negative: false
        }
        const activeIntegrationNode = await cache.getProperty(this.sessionId, CACHE_ACTIVE_INTEGRATION_NODE)
        if (!activeIntegrationNode) {
          await cache.saveProperty(this.sessionId, CACHE_ACTIVE_INTEGRATION_NODE, this.node)
        }
      } else await cache.delProperty(this.sessionId, CACHE_ACTIVE_INTEGRATION_NODE)

      const node: IDataNodeResponse = {
        text: integration.text,
        options: integration.options || [],
        intent: this.node.intent,
        csat,
        derivation,
        data: this.node.data,
        integration: this.node.integration
      }
      return node
    } catch (error) {
      logger.error('[INTEGRATION] Get service', error)
      return this.default()
    }
  }
}
