import { findObjInArray } from '@app/helpers'
import { IDataNodeResponse, IOptions, INode } from '@app/interfaces'

export class TreeNode {
  constructor(public node: INode) {
    this.node = node
  }

  private getOptions(tree: any[]): IOptions[] {
    const childrens = this.node.children
    const options: IOptions[] = []
    for (const i in childrens) {
      const nodeOption = findObjInArray(tree, 'id', childrens[i])
      if (nodeOption) {
        const option: IOptions = {
          id: childrens[i],
          text: nodeOption.option_name
        }
        options.push(option)
      }
    }
    return options
  }

  get(tree: any[]): IDataNodeResponse {
    const children = this.getOptions(tree)
    const node: IDataNodeResponse = {
      text: this.node.message,
      options: children,
      intent: this.node.intent,
      csat: this.node.csat,
      derivation: this.node.derivation,
      data: this.node.data,
      integration: this.node.integration
    }
    return node
  }
}
