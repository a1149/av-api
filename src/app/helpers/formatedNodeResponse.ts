import { TreeNode } from '@root/app/class'
import { IDataNodeResponse, INode, IRequestNode } from '@root/app/interfaces'
import { TBAN, APPM } from '@app/bot-tree'

export default async (integration: IDataNodeResponse | undefined, node: INode, reqBody: IRequestNode): Promise<IDataNodeResponse> => {
  if (integration) return integration

  const formatResponseNode = new TreeNode(node)

  const { channel } = reqBody

  const treeObj = {
    TBAN,
    APPM
  }

  const treeChannel = treeObj[channel]

  return formatResponseNode.get(treeChannel)
}
