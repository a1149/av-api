import Joi from 'joi'

export default async (data: any): Promise<any> => {
  const schema = Joi.object().keys({
    sessionId: Joi.string().required(),
    input: Joi.object().keys({
      type: Joi.string().required(),
      value: Joi.string().required()
    }),
    channel: Joi.string().required()
  })

  return Joi.assert(data, schema)
}
