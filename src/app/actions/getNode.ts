import { logger } from 'av-core-express/lib/handlers'
import { INode, IRequestNode } from '@app/interfaces'
import { TBAN, APPM } from '@app/bot-tree'
import { findObjInArray } from '@app/helpers'
import { CACHE_ACTIVE_INTEGRATION_NODE, INTENT_AV, menuIntention } from '@root/app/constants'
import { cache } from '@root/database'

export default async (body: IRequestNode): Promise<INode> => {
  logger.info('request from getNode', body)

  const { input: { value: question }, channel, sessionId } = body; 

  const activeIntegrationNode: INode = await cache.getProperty(sessionId, CACHE_ACTIVE_INTEGRATION_NODE)

  const isIntentAV = question.substr(-3) === INTENT_AV

  if (activeIntegrationNode && !isIntentAV) return activeIntegrationNode

    if (isIntentAV) await cache.delProperty(sessionId, CACHE_ACTIVE_INTEGRATION_NODE)

  const treeObj = {
    TBAN,
    APPM
  }

  const treeChannel = treeObj[channel]

  const isNode = findObjInArray(treeChannel, 'id', question)

  const idNode = isNode ? body.input.value : menuIntention

  const node = findObjInArray(treeChannel, 'id', idNode)

  return node
}
