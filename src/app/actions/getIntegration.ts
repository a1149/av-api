import { logger } from 'av-core-express/lib/handlers'
import { IDataNodeResponse, INode, IRequestNode } from '@app/interfaces'
import { Integration } from '@app/class'
import { cache } from '@root/database'
import { enabledIntegrations } from '@root/app/constants'

export default async (body: IRequestNode, node: INode): Promise<IDataNodeResponse | undefined> => {
  logger.info('Get Integration' + node.integration)

  const idIntegration = node.integration

  if (!idIntegration || !enabledIntegrations.includes(idIntegration)) return

  const { sessionId } = body

  const attachData = await cache.getProperty(sessionId, 'attachData')

  const integration = new Integration(body, node, attachData.NUP)

  return await integration.get()
}
