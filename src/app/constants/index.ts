export const enabledIntegrations = ['limites', 'resumen']
export const menuIntention = 'CHIT_hola_AV'
export const CACHE_ACTIVE_INTEGRATION_NODE = 'activeIntegrationNode'
export const IS_DEFAULT_RESPONSE = 'DEFAULT_RESPONSE'
export const TBAN_CHANNEL = 'TBAN'
export const APP_CHANNEL = 'APPM'
export const INTENT_AV = '_AV'
