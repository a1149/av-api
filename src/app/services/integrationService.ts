import { IIntegrationRequest } from '@root/app/interfaces/IIntegrations'
import { HttpClient } from 'av-core-express/lib/handlers'
import { IResponse } from 'av-core-express/lib/interfaces'
import { AxiosRequestConfig } from 'axios'

const getHttpClientConfiguration = (): AxiosRequestConfig => {
  return {
    baseURL: process.env.URL_AV_INTEGRATIONS
  }
}

export const getintegration = async (request: IIntegrationRequest): Promise<IResponse> => {
  const { sessionId, nup, channel, integration, option } = request
  const path = `/integrations?sessionId=${sessionId}&channel=${channel}&integration=${integration}&nup=${nup}&option=${option}`
  const responseIntegration = await HttpClient.get(path, getHttpClientConfiguration())
  return responseIntegration.data
}
