/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable quotes */
import app from '@root/app'
import supertest from 'supertest'
import { IRequestNode } from '@app/interfaces'
import { cache } from '@root/database'
import { TBAN_CHANNEL } from '@root/app/constants'

const sessionId = 'mock_interactionsWithException'

const AttachData = {
  sessionId: sessionId,
  NUP: '01234567',
  USR_31_source_channel: TBAN_CHANNEL,
  JSESSIONID: sessionId,
  DOCUMENTO: '23572747',
  APELLIDO: 'COSTANZO',
  NOMBRE: 'EZEQUIEL MARIO',
  SEGMENTOOBP: 'santander',
  EMAILMYA: 'ezeconstanzo@hotmail.com',
  FECHANACIMIENTO: '12/12/1973',
  NROTARJETAVISA: '4509-7902-0362-7348',
  NROTARJETAAMEX: '3777-900463-02098',
  NROTARJETADEBITO: '4517660135288495',
  NROCUENTA: '733-353554/3',
  intencionGoto: '',
  idGEC: '',
  idInternoGEC: '',
  '': 'undefined'
}

const context = {
  USR_31_source_channel: TBAN_CHANNEL,
  USR_30_source_domain: '',
  USR_32_source_derivation: false,
  sessionId: sessionId,
  writtenChannels: false,
  errorMoraTemprana: false,
  moraTemprana: false,
  moraTardia: false,
  montoMoraTemprana: '0',
  token_srvtran: '58f645b4-7c45-4eb5-b398-ce51ed012899',
  nup_srvtran: '01234567',
  session_srvtran: '2B0A370',
  ExCiti: 'N',
  Negocio_cantidadCuentas: 0,
  Negocio_cantidadCuentasAptasPAS: 0,
  Negocio_esClientePAS: false,
  sugirio_mora: false,
  estadisticas_HeaderId: '37857',
  estadisticas_Token: '58f645b4-7c45-4eb5-b398-ce51ed012899',
  nombreWatson: 'santi',
  idGEC: '',
  idInternoGEC: '',
  suggestion_topics: [],
  has_suggestions_topics: false,
  has_options: true,
  dummy_bot: true,
  options: [],
  options_questions: [],
  txtCampania: 'none',
  options_link: [
    false,
    false,
    false,
    false,
    false
  ],
  options_type: [
    'na',
    'na',
    'na',
    'na',
    'na'
  ],
  eventsAnalytics: [
    {
      action: 'Apertura Santi',
      label: 'CTARJ_limites_disponibles_consulta_AV',
      value: 0,
      metric: {},
      dimension: {}
    }
  ],
  notification: '',
  system: {
    dialog_turn_counter: 1
  },
  USR_15_intents_list: [
    {
      intent: 'TARJ_limites_disponibles_consulta_AV',
      confidence: 1
    }
  ],
  USR_15_respuesta_default: 'Hola {#NombreCliente}, me llamo Santi y soy tu Asistente Virtual. 🤖 Para poder ayudarte, seleccioná el motivo por el cual querés consultarme 👇',
  ask_answer_was_useful: false,
  conversation_id: 'NA',
  sugirio_campania: false,
  AttachData: {
    sessionId: sessionId,
    NUP: '01234567',
    USR_31_source_channel: TBAN_CHANNEL,
    JSESSIONID: sessionId,
    DOCUMENTO: '23572747',
    APELLIDO: 'COSTANZO',
    NOMBRE: 'EZEQUIEL MARIO',
    SEGMENTOOBP: 'santander',
    EMAILMYA: 'ezeconstanzo@hotmail.com',
    FECHANACIMIENTO: '12/12/1973',
    NROTARJETAVISA: '4509-7902-0362-7348',
    NROTARJETAAMEX: '3777-900463-02098',
    NROTARJETADEBITO: '4517660135288495',
    NROCUENTA: '733-353554/3',
    intencionGoto: '',
    idGEC: '',
    idInternoGEC: '',
    '': 'undefined'
  }
}

const integrationSuccess = {
  data: {
    code: "SUCCESS",
    statusCode: 200,
    message: "The request has been successful",
    result: {
      text: "Probando respuesta de integración con activeSecuentialDialogue=true y activeIntegrationNode=false mock",
      options: null,
      derivate: false,
      activeSecuentialDialogue: true
    }
  }
}

beforeAll(async (done) => {
  cache.saveProperty(sessionId, 'attachData', AttachData)
  cache.saveObject('01234567_context', context)

  done()
})

jest.mock('axios', () => {
  return {
    create: jest.fn(() => ({
      post: (): any => (integrationSuccess),
      interceptors: {
        request: { use: jest.fn(), eject: jest.fn() },
        response: { use: jest.fn(), eject: jest.fn() }
      }
    }))
  }
})

describe('Interactions test', () => {
  it('OK response', async () => {
    const input = { type: 'option', value: 'TARJ_limites_disponibles_consulta_AV' }
    const mock: IRequestNode = { sessionId: sessionId, input: input, channel: TBAN_CHANNEL }
    cache.saveProperty(sessionId, 'activeIntegrationNode', false)

    const res = await supertest(app)
      .post('/interactions')
      .send(mock)
    expect(res.status).toEqual(200)
  })
})
