import app from '../src/app'
import supertest from 'supertest'
import { IRequestNode } from '../src/app/interfaces/IRequest'

describe('NotFound test', () => {
  it('post 404', async () => {
    const input = { type: 'option', value: 'CLAV_menu_AV' }
    const mock : IRequestNode = { sessionId: 'FOeS6nRD8IwW_yBIY9jM1Zh', input: input }

    const res = await supertest(app)
      .post('/asds')
      .send(mock)
    expect(res.status).toEqual(404)
    expect(res.body.statusCode).toEqual(404)
  })
})
