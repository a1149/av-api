import { TreeNode } from '@app/class'
import { INode, IRequest } from '@app/interfaces'
import { TBAN } from '@app/bot-tree'

import app from '../src/app'
import supertest from 'supertest'
import { cache } from '@root/database'

describe('TreeNode class', () => {
  it('get next node', async () => {
    const mock: INode = {
      id: 'CHIT_hola_AV',
      option_name: '',
      message: 'Hola {#NombreCliente}, me llamo Santi y soy tu Asistente Virtual. 🤖 Para poder ayudarte, seleccioná el motivo por el cual querés consultarme 👇',
      children: [
        'CLAV_menu_AV',
        'CTAS_menu_AV',
        'TARJ_menu_AV',
        'PRES_menu_AV',
        'GRAL_menu_AV'
      ],
      data: [
        {
          text: '',
          link: '',
          type: 'url'
        }
      ],
      derivation: false,
      csat: {
        show: false,
        derivate_when_negative: false
      },
      contact_reason: '',
      intent: 'CHIT_hola_AV'
    }
    const node = new TreeNode(mock)
    const res = node.get(TBAN)
    expect(res.text).toEqual(mock.message)
  })
})

describe('getNodeById exception UNCAUGHT_EXCEPTION', () => {
  it('post interaction', async () => {
    const mock: IRequest = {
      sessionId: 'rFOeS6nRD8IwW_yrrrrBIY9jM1Zh',
      channel: 'TBAN',
      input: {
        type: 'text',
        value: 'TARJ_limites_disponibles_consulta_AV'
      }
    }
    const res = await supertest(app)
      .post('/interactions')
      .send(mock)
    expect(res.status).toEqual(500)
    expect(res.body.statusCode).toEqual(500)
  })
})

describe('getNodeById exception INVALID_BODY', () => {
  it('post interaction', async () => {
    const mock: IRequest = {
      sessionId: 'rFOeS6nRD8IwW_yrrrrBIY9jM1Zh',
      channel: 'TBAN',
      input: {
        type2: 'text',
        value: 'TARJ_limites_disponibles_consulta_AV'
      }
    }
    const res = await supertest(app)
      .post('/interactions')
      .send(mock)
    expect(res.status).toEqual(400)
    expect(res.body.statusCode).toEqual(400)
  })
})

describe('TreeNode class with integration', () => {
  it('post interaction', async () => {
    const mock: IRequest = {
      sessionId: 'rFOeS6nRD8IwW_yrrrrBIY9jM1ZA',
      channel: 'TBAN',
      input: {
        type: 'text',
        value: 'TARJ_limites_disponibles_consulta_AV'
      }
    }
    const attachData = { nup: 123 }
    await cache.saveProperty(mock.sessionId, 'attachData', attachData)
    const res = await supertest(app)
      .post('/interactions')
      .send(mock)
    expect(res.status).toEqual(200)
    expect(res.body.statusCode).toEqual(200)
  })
})

describe('TreeNode class without options', () => {
  it('get next node', async () => {
    const mock: INode = {
      id: 'CHIT_hola_AV',
      option_name: '',
      message: 'Hola {#NombreCliente}, me llamo Santi y soy tu Asistente Virtual. 🤖 Para poder ayudarte, seleccioná el motivo por el cual querés consultarme 👇',
      children: [
        'FAKE_child'
      ],
      data: [
        {
          text: '',
          link: '',
          type: 'url'
        }
      ],
      derivation: false,
      csat: {
        show: false,
        derivate_when_negative: false
      },
      contact_reason: '',
      intent: 'CHIT_hola_AV'
    }
    const node = new TreeNode(mock)
    const res = node.get(TBAN)
    expect(res.text).toEqual(mock.message)
  })
})
