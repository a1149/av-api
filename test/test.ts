import app from '@root/app'
import supertest from 'supertest'

describe('Status endpoint', () => {
  it('Status active', async () => {
    const res = await supertest(app)
      .get('/live')
    expect(res.status).toEqual(200)
  })
})
