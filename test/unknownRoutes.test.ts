import app from '@root/app'
import supertest from 'supertest'
import { IRequestNode } from '@app/interfaces'

describe('unknownRoutes test', () => {
  it('post 404', async () => {
    const input = { type: 'option', value: 'CLAV_menu_AV' }
    const mock : IRequestNode = { sessionId: 'FOeS6nRD8IwW_yBIY9jM1Zh', input: input }

    const res = await supertest(app)
      .post('/asds')
      .send(mock)
    expect(res.status).toEqual(404)
    expect(res.body.statusCode).toEqual(404)
  })
})
